// ************************************************************
// STL.java			Author: Nikodemos Koutsoheras
// 
// Represents a set of t forming a mesh in 3D space.
// ************************************************************
package entities;

import java.awt.Color;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Stack;

import main.Display;

public class STL {

	private Triangle3D[] triangles;
	private Stack<Point3D> vertices = new Stack<Point3D>();
	private Stack<Color> colors = new Stack<Color>();
	
	public void shade(double[] source) {
		for (Triangle3D t : triangles) {
			t.shade(source);
		}
	}
	
	public STL(Triangle3D[] t) {
		triangles = t;
	}
	
	public STL(String fName) {
		ArrayList<Triangle3D> t = new ArrayList<Triangle3D>();
		InputStream is = this.getClass().getResourceAsStream("/STL/" + fName + ".stl");
		BufferedReader in = new BufferedReader(new InputStreamReader(is));
		String line;
		try {
			line = in.readLine();
			
			while ((line = in.readLine()) != null) {
				if (line.indexOf("vertex") > -1) {
					line = line.substring(line.indexOf("x") + 1).trim().toLowerCase();
//					System.out.println(line);
					String[] numStrings = line.split(" ");
					double[] coords = new double[3];
					for (int i = 0; i < 3; i ++) { // i <3 you too
//						System.out.print(numStrings[i] + " to ");
						coords[i] = Double.parseDouble(numStrings[i].substring(0, numStrings[i].indexOf('e'))) * Math.pow(10, Integer.parseInt(numStrings[i].substring(numStrings[i].indexOf('e') + 1)));
//						System.out.println(coords[i]);
					}
						vertices.push(new Point3D(coords[0], coords[1], coords[2]));
				}
				if (line.indexOf("normal") > -1) {
					line = line.substring(line.indexOf("l") + 1).trim().toLowerCase();
//					System.out.println("Color " + line);
					String[] numStrings = line.split(" ");
					double[] coords = new double[3];
					for (int i = 0; i < 3; i ++) { // i <3 you too
//						System.out.print(numStrings[i] + " to ");
						coords[i] = Double.parseDouble(numStrings[i].substring(0, numStrings[i].indexOf('e'))) * Math.pow(10, Integer.parseInt(numStrings[i].substring(numStrings[i].indexOf('e') + 1)));
//						System.out.println(coords[i]);
					}
						colors.push(new Color((int) (200 * Math.abs(coords[0])), (int) (200 * Math.abs(coords[1])), (int) (200 * Math.abs(coords[2]))));
				}
				if (vertices.size() == 3) {
					t.add(new Triangle3D(vertices.pop(), vertices.pop(), vertices.pop()));
					//					t.get(t.size()).setColor(colors.pop());
				}
			}
		} catch (NumberFormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println("Done parsing.");
		//		triangles = t.toArray(triangles); TRY THIS LATER
		triangles = new Triangle3D[t.size()];
		for (int i = 0; i < t.size(); i ++) {
			triangles[i] = t.get(i);
		}
	}

	public STL clone() {
		Triangle3D[] newTriangles = new Triangle3D[triangles.length];
		for (int i = 0; i < newTriangles.length; i ++) {
			newTriangles[i] = triangles[i].clone();
		}
		return new STL(newTriangles);
	}
	
	public void sortTrianglesByDistance(Point3D p) {
		for (Triangle3D t : triangles) {
			t.calculateClosestPoint(Display.focalPoint);
		}
		quicksortTriangles(0, triangles.length - 1);

	}

	private void quicksortTriangles(int lower, int higher) {
		int i = lower;
		int j = higher;
		double pivot = triangles[lower + (higher - lower) / 2].getClosestDist();

		while (i <= j) {
			while (triangles[i].getClosestDist() > pivot) {
				i ++;
			}
			while (triangles[j].getClosestDist() < pivot) {
				j --;
			}

			if (i <= j) {
				Triangle3D temp = triangles[i];
				triangles[i] = triangles[j];
				triangles[j] = temp;
				i++;
				j--;
			}
		}
		
		if (lower < j) {
			quicksortTriangles(lower, j);
		}
		if (i < higher) {
			quicksortTriangles(i, higher);
		}
	}




	public void printTriangles() {
		for (Triangle3D t : triangles) {
			System.out.println(t.getClosestDist());
		}
	}
	
	public Triangle3D[] triangles() {
		return triangles;
	}
	
	public String toString() {
		String retString = "";
		System.out.println("There are " + triangles.length + " t");
		for (Triangle3D t : triangles) {
			retString += t + "\n";
		}
		return retString;
	}
	
}
