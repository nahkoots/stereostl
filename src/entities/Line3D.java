package entities;

public class Line3D {

	Point3D[] points = new Point3D[2];
	
	public Line3D (Point3D point, Point3D point2) {
		points[0] = point;
		points[1] = point2;
	}
	
	public Point3D[] points() {
		return points;
	}
	
}
