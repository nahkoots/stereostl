package entities;

public class Point3D {
	
	private double[] coordinates = new double[3];
	private double[] apparentCoordinates = new double[2];

	public Point3D(double x, double y, double z) {
		coordinates[0] = x;
		coordinates[1] = y;
		coordinates[2] = z;
	}
	
	public double x() {
		return coordinates[0];
	}
	
	public double y() {
		return coordinates[1];
	}
	
	public double z() {
		return coordinates[2];
	}
	
	public void setX(double d) {
		coordinates[0] = d;
	}
	
	public void setY(double d) {
		coordinates[1] = d;
	}
	
	public void setZ(double d) {
		coordinates[2] = d;
	}
	
	public void setApparentX(double d) {
		apparentCoordinates[0] = d;
	}
	
	public void setApparentY(double d) {
		apparentCoordinates[1] = d;
	}
	
	public double distanceTo(Point3D p) {
		return Math.sqrt(Math.pow(coordinates[0] - p.x(), 2) + Math.pow(coordinates[1] - p.y(), 2) + Math.pow(coordinates[2] - p.z(), 2));
	}
	
	public double[] apparentCoordinates() {
		double[] ret = {apparentCoordinates[0], apparentCoordinates[1]};
		return ret;
	}
	
	public String toString() {
		return coordinates[0] + ", " + coordinates[1] + ", " + coordinates[2];
	}
	
}
