// ************************************************************
// Triangle3D.java			Author: Nikodemos Koutsoheras
// 
// Represents a filled triangle in 3D space.
// ************************************************************
package entities;

import java.awt.Color;
import java.util.Comparator;

import main.Display;

public class Triangle3D implements Comparator<Triangle3D> {

	Point3D[] points = new Point3D[3];
	Color color = new Color(127, 127, 127);
	Color shadedColor = Color.CYAN;
	int shade = 0; // goes from -64 to +64
	double distClosestPointToFocus;
	int closestPointIndex;
	
	double[] normal = new double[3];
	double[] firstNormal = new double[3];
	boolean doDraw = true;
	double depth = 0;
	
	public Triangle3D(Point3D point, Point3D point2, Point3D point3) {
		points[0] = point;
		points[1] = point2;
		points[2] = point3;
	}
	
	public double calculateClosestPoint(Point3D p) {
		double minDist = 999999;
		double maxDist = 0;
		for (int i = 0; i < 3; i ++) {
			double temp = points[i].distanceTo(p);
			if (temp < minDist) {
				minDist = temp;
				closestPointIndex = i;
			}
		}
		distClosestPointToFocus = minDist;
//		System.out.println(points[closestIndex] + " is the closest of " + points[0] + ", " + points[1] + ", " + points[2]);
		return minDist;
	}
	
	public double getClosestDist() {
		return distClosestPointToFocus;
	}
	
	public double angleToPoint() {
		return depth;
		
	}
	
	// ---------------------------------------------
	// Sets the normal vector for the triangle
	// ---------------------------------------------
	public void setNormal(double[] d) {
		firstNormal = d;
	}

	// ---------------------------------------------
	// Calculates and returns the normal vector for the triangle
	// ---------------------------------------------
	public double[] getNormal() {
		double[] vectorAB = {points[1].x() - points[0].x(), points[1].y() - points[0].y(), points[1].z() - points[0].z()};
		double[] vectorAC = {points[2].x() - points[0].x(), points[2].y() - points[0].y(), points[2].z() - points[0].z()};
		double[] normalABC = {vectorAB[1] * vectorAC[2] - vectorAB[2] * vectorAC[1],
				vectorAB[2] * vectorAC[0] - vectorAB[0] * vectorAC[2],
				vectorAB[0] * vectorAC[1] - vectorAB[1] * vectorAC[0]};

		double vectorLength = Math.sqrt(Math.pow(normalABC[0], 2) + Math.pow(normalABC[1], 2) + Math.pow(normalABC[2], 2));
		for (int i = 0; i < 3; i ++) {
			normalABC[i] /= vectorLength;
		}

		//		System.out.println(normalABC[0] + ", " + normalABC[1] + ", " + normalABC[2]);
		normal = normalABC;
		return normalABC;
		//		return normal;
	}

	public double[] getNormalNoCalc() {
		return normal;
	}
	
	public Point3D[] points() {
		return points;
	}
	
	public void shade(double[] source) {
		this.getNormal();
		double mag = (normal[0] * source[0] + normal[1] * source[1] + normal[2] * source[2]);
		double thing1 = Math.sqrt(Math.pow(normal[0], 2) + Math.pow(normal[1], 2) + Math.pow(normal[2], 2));
		double thing2 = Math.sqrt(Math.pow(source[0], 2) + Math.pow(source[1], 2) + Math.pow(source[2], 2));
		double val = mag / (thing1 * thing2);
		if (val > 1) {
			System.out.println("Truncating " + val + " to 1.");
			val = 1;
		}
		if (val < -1) {
			System.out.println("Truncating " + val + " to -1.");
			val = -1;
		}
		
		double angle = Math.acos(val);
		shade = (int) ((angle - (Math.PI / 2)) * ((64 /( Math.PI / 2))));
//		System.out.println("Shade is " + shade);
//		System.out.println(normal[0] + ", " + normal[1] + ", " + normal[2]);
		shadedColor = new Color(color.getRed() + shade, color.getBlue() + shade, color.getGreen() + shade);
		
	}
	
	public boolean isVisibleFromPoint(Point3D p) {
//		this.getNormal();
		double[] source = {points[closestPointIndex].x() - p.x(), points[closestPointIndex].y() - p.y(), points[closestPointIndex].z() - p.z()};
		double mag = (normal[0] * source[0] + normal[1] * source[1] + normal[2] * source[2]);
		double thing1 = Math.sqrt(Math.pow(normal[0], 2) + Math.pow(normal[1], 2) + Math.pow(normal[2], 2));
		double thing2 = Math.sqrt(Math.pow(source[0], 2) + Math.pow(source[1], 2) + Math.pow(source[2], 2));
		double val = mag / (thing1 * thing2);
		if (val > 1) {
			System.out.println("Truncating " + val + " to 1.");
			val = 1;
		}
		if (val < -1) {
			System.out.println("Truncating " + val + " to -1.");
			val = -1;
		}
		
		double angle = Math.acos(val);
		return (angle < Math.PI / 2);
	}
	
	public void setColor(Color c) {
		color = c;
	}
	
	public Color getColor() {
		return color;
	}
	
	public Color getShadedColor() {
		return shadedColor;
	}
	
	public String toString() {
		String retString = "";
		for (Point3D p : points) {
			retString += p + ", ";
		}
		return retString.substring(0, retString.length() - 1);
	}
	
	public Triangle3D clone() {
		return new Triangle3D(points[0], points[1], points[2]);
	}

//	public int compareTo(Object arg0) {
//		double baseComparison = distClosestPointToFocus - ((Triangle3D) arg0).calculateClosestPoint(Display.focalPoint); 
//		if (Math.abs(baseComparison) < .00001) {
//			System.out.println("Truncating comparison value from " + baseComparison + " to 0.");
//			baseComparison = 0;
//		}
//		System.out.println((distClosestPointToFocus - ((Triangle3D) arg0).calculateClosestPoint(Display.focalPoint)));
//		return (int) baseComparison;
//	}

	@Override
	public int compare(Triangle3D arg0, Triangle3D arg1) {
		double thing1 = arg0.getClosestDist() - arg1.getClosestDist();
		if (Math.abs(thing1) < .0001) {
			thing1 = 0;
		}
		return (int) (thing1);
	}
	
}
