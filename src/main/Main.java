package main;
// Display.java			Author: Nikodemos Koutsoheras
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.JFrame;
import javax.swing.JPanel;

import entities.Point3D;

public class Main extends JFrame implements KeyListener {
	// 800 x 700 is proper
	public static final int SCREEN_HSIZE = 1200;
	public static final int SCREEN_VSIZE = 800;
	public static final int DEBUG_HSIZE = 500;
	public static double EYE_ORIGIN_OFFSET = 10;

	public static final int KEY_LEFT = 0;
	public static final int KEY_UP = 1;
	public static final int KEY_RIGHT = 2;
	public static final int KEY_DOWN = 3;
	public static final int KEY_Q = 4;
	public static final int KEY_W = 5;
	public static final int KEY_E = 6;
	public static final int KEY_A = 7;
	public static final int KEY_S = 8;
	public static final int KEY_D = 9;
	public static final int KEY_Z = 10;
	public static final int KEY_U = 11;
	public static final int KEY_I = 12;
	public static final int KEY_O = 13;
	public static final int KEY_J = 14;
	public static final int KEY_K = 15;
	public static final int KEY_L = 16;
	public static final int KEY_C = 17;
	public static final int KEY_F = 18;
	public static final int KEY_SHIFT = 19;

	private boolean[] pressed = new boolean[20];

	private boolean running = true;
	private boolean paused = false;

	final double GAME_HERTZ = 20.0;
	final double TIME_BETWEEN_UPDATES = 1000000000 / GAME_HERTZ;
	final int MAX_UPDATES_BEFORE_RENDER = 5;
	final double TARGET_FPS = 60;
	final double TARGET_TIME_BETWEEN_RENDERS = 1000000000 / TARGET_FPS;

	private static final long serialVersionUID = 1L;
	private Display leftEye, rightEye;
	private JPanel contentPane;

	// ------------------------------------------------------------
	// Constructor - sets up the window and the game's panel
	// ------------------------------------------------------------
	public Main() {
		super("CAST STL");
		setBackground(Color.BLACK);
		contentPane = new JPanel();
		contentPane.setLayout(new GridLayout());
		leftEye = new Display(true);
		leftEye.setPreferredSize(new Dimension(SCREEN_HSIZE / 2, SCREEN_VSIZE));
		leftEye.setFocalPoint(new Point3D(-EYE_ORIGIN_OFFSET, 0, 160));
		rightEye = new Display(false);
		rightEye.setPreferredSize(new Dimension(SCREEN_HSIZE / 2, SCREEN_VSIZE));
		rightEye.setFocalPoint(new Point3D(EYE_ORIGIN_OFFSET, 0, 160));
		contentPane.add(leftEye, BorderLayout.WEST);
		contentPane.add(rightEye, BorderLayout.EAST);




		this.setContentPane(contentPane);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setResizable(true);
		addKeyListener(this);
		this.pack();
		this.setVisible(true);
		this.setSize(SCREEN_HSIZE, SCREEN_VSIZE);

		runMainLoop();

	}

	// ------------------------------------------------------------
	// Main loop for the game
	// Written by Eli Delventhal
	// ------------------------------------------------------------
	private void gameLoop() {
		double lastUpdateTime = System.nanoTime();
		double lastRenderTime = System.nanoTime();

		//Simple way of finding FPS.
		int lastSecondTime = (int) (lastUpdateTime / 1000000000);

		while (running)
		{
			double now = System.nanoTime();
			int updateCount = 0;

			if (!paused)
			{

				//Do as many game updates as we need to, potentially playing catchup.
				while( now - lastUpdateTime > TIME_BETWEEN_UPDATES && updateCount < MAX_UPDATES_BEFORE_RENDER )
				{
					updateGame();
					lastUpdateTime += TIME_BETWEEN_UPDATES;
					updateCount++;
				}

				//If for some reason an update takes forever, we don't want to do an insane number of catchups.
				//If you were doing some sort of game that needed to keep EXACT time, you would get rid of this.
				if ( now - lastUpdateTime > TIME_BETWEEN_UPDATES)
				{
					lastUpdateTime = now - TIME_BETWEEN_UPDATES;
				}

				drawGame();
				lastRenderTime = now;

				//Update the frames we got.
				int thisSecond = (int) (lastUpdateTime / 1000000000);
				if (thisSecond > lastSecondTime)
				{
					//					System.out.println("NEW SECOND " + thisSecond + " " + frameCount);
					lastSecondTime = thisSecond;
				}

				//Yield until it has been at least the target time between renders. This saves the CPU from hogging.
				while ( now - lastRenderTime < TARGET_TIME_BETWEEN_RENDERS && now - lastUpdateTime < TIME_BETWEEN_UPDATES)
				{
					Thread.yield();

					//This stops the app from consuming all your CPU. It makes this slightly less accurate, but is worth it.
					//You can remove this line and it will still work (better), your CPU just climbs on certain OSes.
					//FYI on some OS's this can cause pretty bad stuttering. Scroll down and have a look at different peoples' solutions to this.
					try {Thread.sleep(1);} catch(Exception e) {} 

					now = System.nanoTime();
				}
			}
		}
	}

	// ------------------------------------------------------------
	// Updates the game's state, including all of its objects
	// ------------------------------------------------------------
	private void updateGame()
	{
		if (pressed[KEY_F]) { 
			if (pressed[KEY_SHIFT]) {
				EYE_ORIGIN_OFFSET -= .25;
			} else {
				EYE_ORIGIN_OFFSET += .25;
			}
			leftEye.setFocalPoint(new Point3D(-EYE_ORIGIN_OFFSET, 0, 160));
			rightEye.setFocalPoint(new Point3D(EYE_ORIGIN_OFFSET, 0, 160));
		}

		leftEye.setPressed(pressed);
		leftEye.update();
		rightEye.update();
	}

	// ------------------------------------------------------------
	// Redraws the game
	// ------------------------------------------------------------
	private void drawGame()
	{
		leftEye.repaint();
		rightEye.repaint();
	}


	// ------------------------------------------------------------
	// Starts a thread for the main loop
	// ------------------------------------------------------------
	public void runMainLoop() {

		new Thread() {
			public void run() {
				gameLoop();
			}
		}.start();

	}

	private void switchKey(int key, boolean state) {
//				System.out.println("Key " + key + (char) key);
		switch (key) {
		case 16: pressed[KEY_SHIFT] = state; break;
		case 81: pressed[KEY_Q] = state; break;
		case 87: pressed[KEY_W] = state; break;
		case 69: pressed[KEY_E] = state; break;
		case 65: pressed[KEY_A] = state; break;
		case 83: pressed[KEY_S] = state; break;
		case 68: pressed[KEY_D] = state; break;
		case 85: pressed[KEY_U] = state; break;
		case 73: pressed[KEY_I] = state; break;
		case 79: pressed[KEY_O] = state; break;
		case 74: pressed[KEY_J] = state; break;
		case 75: pressed[KEY_K] = state; break;
		case 76: pressed[KEY_L] = state; break;
		case 67: pressed[KEY_C] = state; break;
		case 70: pressed[KEY_F] = state; break;
		}

	}


	// ------------------------------------------------------------
	// Tells displays that a key is pressed
	// ------------------------------------------------------------
	@Override
	public void keyPressed(KeyEvent evt) {
		switchKey(evt.getKeyCode(), true);
	}

	// ------------------------------------------------------------
	// Tells displays that a key has been released
	// ------------------------------------------------------------
	@Override
	public void keyReleased(KeyEvent evt) {
		switchKey(evt.getKeyCode(), false);	

	}

	@Override
	public void keyTyped(KeyEvent arg0) {
		// TODO Auto-generated method stub

	}
	
	// ------------------------------------------------------------
	// Starts everything
	// ------------------------------------------------------------
	public static void main(String[] args) {
		@SuppressWarnings("unused")
		Main game = new Main();	
	}
}

