package main;
import java.awt.Color;
// Display.java			Author: Nikodemos Koutsoheras
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Polygon;
import java.awt.Toolkit;
import java.util.ArrayList;

import javax.swing.JPanel;

import entities.Point3D;
import entities.STL;
import entities.Triangle3D;

public class Display extends JPanel {
	private static final long serialVersionUID = 1L;
	private static final double MOVE = 1;
	private static final double ROTATE = Math.toRadians(5);
	private static ArrayList<STL> selectedSTLs = new ArrayList<STL>(); // the classes share the STL list so there's no discrepancy between eyes, also helps calc time
	private boolean masterClient;
	public static Point3D focalPoint = new Point3D(0, 0, 4500);
	private double projectionPlane = 350;
	private static boolean[] isPressed = new boolean[20]; // prevents discrepancies between pressed buttons
	private static boolean showCenterPoint = false;
	private static Point3D centerPoint;
	private double[] sun = {1, -1, 1}; // light source vector
	private Image offImage, nextFrame;
	private Graphics offGraphics;
	private boolean reRender = true;



	// ------------------------------------------------------------
	// Constructor
	// ------------------------------------------------------------
	public Display(boolean b) {
		masterClient = b;
		centerPoint = new Point3D(0, 0, 0);
		setBackground(Color.GRAY);

		if (masterClient) {
			selectedSTLs.add(new STL("teapot"));
//						selectedSTLs.add(new STL("lander"));
//									selectedSTLs.add(new STL("terrain"));
//						selectedSTLs.add(new STL("spinner"));
		}
		//		System.out.print(selectedSTLs);
		for (STL mesh : selectedSTLs) {
			mesh.shade(sun);
		}
		nextFrame = offImage;
	}

	// ---------------------------------------------
	// Updates the image based on user input
	// ---------------------------------------------
	public void update() {
		boolean[] tempPressed = isPressed.clone();
		for (boolean b : tempPressed) {
			if (b) {
				reRender = true;
			}
		}
		if (!tempPressed[Main.KEY_SHIFT]) {
			if (tempPressed[Main.KEY_A] || tempPressed[Main.KEY_D] ||
					tempPressed[Main.KEY_W] || tempPressed[Main.KEY_S] ||
					tempPressed[Main.KEY_Q] || tempPressed[Main.KEY_E]) {
				for (STL s : selectedSTLs) {
					for (Triangle3D t : s.triangles()) {
						for (Point3D p : t.points()) {
							if (tempPressed[Main.KEY_A]) {
								p.setX(p.x() + MOVE);
							}
							if (tempPressed[Main.KEY_D]) {
								p.setX(p.x() - MOVE);
							}
							if (tempPressed[Main.KEY_W]) {
								p.setY(p.y() + MOVE);
							}
							if (tempPressed[Main.KEY_S]) {
								p.setY(p.y() - MOVE);
							}
							if (tempPressed[Main.KEY_Q]) {
								p.setZ(p.z() + MOVE);
							}
							if (tempPressed[Main.KEY_E]) {
								p.setZ(p.z() - MOVE);
							}

						}
					}
				}
			}
			if (tempPressed[Main.KEY_C]) {
				showCenterPoint = true;
			}

		} else { // shift is pressed, use modifer rotate
			if (tempPressed[Main.KEY_A] || tempPressed[Main.KEY_D] ||
					tempPressed[Main.KEY_W] || tempPressed[Main.KEY_S] ||
					tempPressed[Main.KEY_Q] || tempPressed[Main.KEY_E]) {
				int numPoints = 0;
				double avgX = 0, avgY = 0, avgZ = 0;
				for (STL s : selectedSTLs) {
					numPoints = s.triangles().length * 3;
					for (Triangle3D t2 : s.triangles()) {
						for (Point3D p : t2.points()) {
							avgX += p.x();
							avgY += p.y();
							avgZ += p.z();
						}
					}
				}
				avgX /= numPoints;
				avgY /= numPoints;
				avgZ /= numPoints;
				centerPoint = new Point3D(avgX, avgY, avgZ);
				for (STL s : selectedSTLs) {
					for (Triangle3D t : s.triangles()) {
						for (Point3D p : t.points()) {
							if (tempPressed[Main.KEY_A]) { // rotate clockwise around Y axis
								double tempX = Math.cos(-ROTATE) * (p.x() - centerPoint.x()) - Math.sin(-ROTATE) * (p.z() - centerPoint.z()) + centerPoint.x();
								double tempZ = Math.sin(-ROTATE) * (p.x() - centerPoint.x()) + Math.cos(-ROTATE) * (p.z() - centerPoint.z()) + centerPoint.z();
								p.setX(tempX);
								p.setZ(tempZ);
							}
							if (tempPressed[Main.KEY_D]) { // rotate counterclockwise around Y axis
								double tempX = Math.cos(ROTATE) * (p.x() - centerPoint.x()) - Math.sin(ROTATE) * (p.z() - centerPoint.z()) + centerPoint.x();
								double tempZ = Math.sin(ROTATE) * (p.x() - centerPoint.x()) + Math.cos(ROTATE) * (p.z() - centerPoint.z()) + centerPoint.z();
								p.setX(tempX);
								p.setZ(tempZ);
							}
							if (tempPressed[Main.KEY_W]) { // rotate clockwise around X axis
								double tempZ = Math.cos(ROTATE) * (p.z() - centerPoint.z()) - Math.sin(ROTATE) * (p.y() - centerPoint.y()) + centerPoint.z();
								double tempY = Math.sin(ROTATE) * (p.z() - centerPoint.z()) + Math.cos(ROTATE) * (p.y() - centerPoint.y()) + centerPoint.y();
								p.setZ(tempZ);
								p.setY(tempY);
							}
							if (tempPressed[Main.KEY_S]) { // rotate counterclockwise around X axis
								double tempZ = Math.cos(-ROTATE) * (p.z() - centerPoint.z()) - Math.sin(-ROTATE) * (p.y() - centerPoint.y()) + centerPoint.z();
								double tempY = Math.sin(-ROTATE) * (p.z() - centerPoint.z()) + Math.cos(-ROTATE) * (p.y() - centerPoint.y()) + centerPoint.y();
								p.setZ(tempZ);
								p.setY(tempY);

							}
							if (tempPressed[Main.KEY_Q]) { // rotate clockwise around Z axis
								double tempX = Math.cos(-ROTATE) * (p.x() - centerPoint.x()) - Math.sin(-ROTATE) * (p.y() - centerPoint.y()) + centerPoint.x();
								double tempY = Math.sin(-ROTATE) * (p.x() - centerPoint.x()) + Math.cos(-ROTATE) * (p.y() - centerPoint.y()) + centerPoint.y();
								p.setX(tempX);
								p.setY(tempY);
							}
							if (tempPressed[Main.KEY_E]) { // rotate counterclockwise around Z axis
								double tempX = Math.cos(ROTATE) * (p.x() - centerPoint.x()) - Math.sin(ROTATE) * (p.y() - centerPoint.y()) + centerPoint.x();
								double tempY = Math.sin(ROTATE) * (p.x() - centerPoint.x()) + Math.cos(ROTATE) * (p.y() - centerPoint.y()) + centerPoint.y();
								p.setX(tempX);
								p.setY(tempY);
							}		
						}
					}
				}
			}
			if (tempPressed[Main.KEY_C]) {
				showCenterPoint = false;
			}
		}
		for (STL mesh : selectedSTLs) {
			mesh.shade(sun);
			mesh.sortTrianglesByDistance(focalPoint);
		}
		
		genFrame();
	}

	// ---------------------------------------------
	// Generates the next frame of the image
	// ---------------------------------------------
	private void genFrame() {
		if (reRender) {
			offImage = null;
			offImage = createImage(getSize().width, getSize().height);
			offGraphics = offImage.getGraphics();
			for (STL s : selectedSTLs) {
				for (Triangle3D t : s.triangles()) {
					for (Point3D p : t.points()) {
						p.setApparentX((projectionPlane * (focalPoint.x() - p.x())) / (focalPoint.z() - p.z()));
						p.setApparentY((projectionPlane * (focalPoint.y() - p.y())) / (focalPoint.z() - p.z()));
					}
					int[] xCoords = new int[3];
					int[] yCoords = new int[3];
					for (int i = 0; i < 3; i ++) {
						xCoords[i] = (int) (getWidth() / 2 + t.points()[i].apparentCoordinates()[0]) + ((masterClient)? 1 : -1) * (int) (5 * Main.EYE_ORIGIN_OFFSET);
						yCoords[i] = (int) (getHeight() / 2 + t.points()[i].apparentCoordinates()[1]);
					}
					//				if (((showCenterPoint)? t.getNormal() : t.getNormalNoCalc())[2] > 0) {
					//					g.setColor(Color.RED);
					//				} else {
					offGraphics.setColor(t.getShadedColor());
					if (t.isVisibleFromPoint(focalPoint) && t.points()[0].z() < focalPoint.z() && t.points()[1].z() < focalPoint.z() && t.points()[2].z() < focalPoint.z()) {
						offGraphics.fillPolygon(new Polygon(xCoords, yCoords, 3));
						
					}
					//				}


					//				g.setColor(t.getColor());
					//				g.setColor(Color.BLACK);

				}
			}
			offGraphics.setColor(Color.RED);
			if (showCenterPoint) {
				int numPoints = 0;
				double avgX = 0, avgY = 0, avgZ = 0;
				for (STL s : selectedSTLs) {
					numPoints = s.triangles().length * 3;
					for (Triangle3D t2 : s.triangles()) {
						for (Point3D p : t2.points()) {
							avgX += p.x();
							avgY += p.y();
							avgZ += p.z();
						}
					}
				}
				avgX /= numPoints;
				avgY /= numPoints;
				avgZ /= numPoints;
				centerPoint = new Point3D(avgX, avgY, avgZ);
				offGraphics.fillRect((int) (getWidth() / 2 + (projectionPlane * (focalPoint.x() - centerPoint.x())) / (focalPoint.z() - centerPoint.z())) + ((masterClient)? 1 : -1) * (int) (5 * Main.EYE_ORIGIN_OFFSET), (int) (getHeight() / 2 + (projectionPlane * (focalPoint.y() - centerPoint.y())) / (focalPoint.z() - centerPoint.z()) - 2), 4, 4);
			}
			reRender = false;
		}
	}
	
	// ------------------------------------------------------------
	// Paints the image
	// ------------------------------------------------------------
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		Toolkit.getDefaultToolkit().sync(); // makes swing run smoothly on Linux
		g.fillRect(0, 0, getWidth(), 10);
		g.clearRect(0, 0, this.getWidth(), this.getHeight());
		g.drawImage(offImage, 0, 0, this);
	}

	// ------------------------------------------------------------
	// Sets the state of a given button
	// ------------------------------------------------------------
	public void setPressed(boolean[] keys) {
		isPressed = keys;
	}

	// ------------------------------------------------------------
	// Sets the focal point (used to offset eyes)
	// ------------------------------------------------------------
	public void setFocalPoint(Point3D p) {
		focalPoint = p;
	}

}